/*
 * Даны неотрицательные целые числа n, k и массив целых чисел из диапазона [0..109] размера n.
 * Требуется найти k-ю порядковую статистику. т.е. напечатать число, которое бы стояло на позиции с индексом k ∈[0..n-1] в отсортированном массиве.
 * Напишите нерекурсивный алгоритм.
 * Требования к дополнительной памяти: O(n).
 * Требуемое среднее время работы: O(n).
 * Функцию Partition следует реализовывать методом прохода двумя итераторами в одном направлении.
 * Описание для случая прохода от начала массива к концу:
 * Выбирается опорный элемент.
 * Опорный элемент меняется с последним элементом массива.
 * Во время работы Partition в начале массива содержатся элементы, не бОльшие опорного.
 * Затем располагаются элементы, строго бОльшие опорного. В конце массива лежат нерассмотренные элементы. Последним элементом лежит опорный.
 * Итератор (индекс) i указывает на начало группы элементов, строго бОльших опорного.
 * Итератор j больше i, итератор j указывает на первый нерассмотренный элемент.
 * Шаг алгоритма. Рассматривается элемент, на который указывает j.
 * Если он больше опорного, то сдвигаем j.
 * Если он не больше опорного, то меняем a[i] и a[j] местами, сдвигаем i и сдвигаем j.
 * В конце работы алгоритма меняем опорный и элемент, на который указывает итератор i.
 */

#include <cassert>
#include <iostream>

int median(const int * a, int p, int r);
int get_k_stat(int * array, size_t size_mass, int index_position);
int swap_sub_mass(int * array, int left_index, int right_index);

int main() {
    size_t size_arr = 0;
    int index_position = 0;

    std::cin >> size_arr >> index_position;
    assert(size_arr > 0);
    assert(index_position >= 0);

    int *array = new int[size_arr];

    for(int i = 0; i < size_arr; i++) {
        std::cin >> array[i];
    }

    std::cout << get_k_stat(array, size_arr, index_position);

    delete[] array;
    return 0;
}

int median(const int * a, int p, int r) {
    int mid = (p + r) / 2;

    if (a[p] > a[r]) {
        if (a[r] > a[mid]) {
            return r;
        } else {
            return mid;
        }
    } else if (a[p] > a[mid]) {
        return p;
    } else {
        return mid;
    }
}

int get_k_stat(int *array, size_t size_mass, int index_position) {
    int left_index = 0;
    size_t right_index = size_mass - 1;

    int q = swap_sub_mass(array, left_index, right_index);
    while (q != index_position) {
        if(index_position < q) {
            right_index = q - 1;
        } else {
            left_index = q + 1;
        }

        q = swap_sub_mass(array, left_index, right_index);
    }

    return array[q];
}

int swap_sub_mass(int *array, int left_index, int right_index) {
    std::swap(array[right_index], array[median(array, left_index, right_index)]);
    int j = left_index;

    for (int i = left_index; i < right_index; i++) {
        if (array[i] < array[right_index]) {
            std::swap(array[i], array[j]);
            j++;
        }
    }

    std::swap(array[j], array[right_index]);
    return j;
}
