/*
 * В одной военной части решили построить в одну шеренгу по росту.
 * Т.к. часть была далеко не образцовая, то солдаты часто приходили не вовремя,
 * то их и вовсе приходилось выгонять из шеренги за плохо начищенные сапоги.
 * Однако солдаты в процессе прихода и ухода должны были всегда быть выстроены по росту –
 * сначала самые высокие, а в конце – самые низкие.
 * За расстановку солдат отвечал прапорщик, который заметил интересную особенность –
 * все солдаты в части разного роста.
 * Ваша задача состоит в том, чтобы помочь прапорщику правильно расставлять солдат, а именно
 * для каждого приходящего солдата указывать, перед каким солдатом в строе он должен становится.
 * Требуемая скорость выполнения команды - O(log n).
 */

#include <iostream>
#include <stack>
#include <vector>

template<typename T>
struct Node {
    // Ключ
    T key;
    // Количество узлов
    int nodes;
    // Высота
    int height;
    Node* left;
    Node* right;

    explicit Node(int key): key{key}, height{1}, nodes{1}, left{nullptr}, right{nullptr} {}
};

template<typename T, typename Comparator = std::less_equal<T>>
class AVL_tree {
public:
    int height(Node<T> *p);
    int Nodes(Node<T> *p);
    int bfactor(Node<T> *p);
    void fixNodes(Node<T> *p);
    void fixHeight(Node<T> *p);
    Node<T>* rotate_right(Node<T>* p);
    Node<T>* rotate_left(Node<T>* q);
    Node<T>* balance(Node<T>* p);
    Node<T>* insert(Node<T>* p, T k, int& position, Comparator cmp = Comparator());
    Node<T>* find_min(Node<T>* p);
    Node<T>* remove_min(Node<T>* p);
    Node<T>* Remove(Node<T>* p, int position);
};

template<typename T, typename Comparator>
int AVL_tree<T, Comparator>::height(Node<T> *p) {
    return p? p->height: 0;
}

template<typename T, typename Comparator>
int AVL_tree<T, Comparator>::bfactor(Node<T> *p) {
    return height(p->right) - height(p->left);
}

template<typename T, typename Comparator>
int AVL_tree<T, Comparator>::Nodes(Node<T> *p) {
    if (p == nullptr) {
        return 0;
    } else {
        return p->nodes;
    }
}

template<typename T, typename Comparator>
void AVL_tree<T, Comparator>::fixNodes(Node<T> *p) {
    p->nodes = Nodes(p->left) + Nodes(p->right) + 1;
}

template<typename T, typename Comparator>
void AVL_tree<T, Comparator>::fixHeight(Node<T> *p) {
    p->height = std::max(height(p->right), height(p->left)) + 1;
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::rotate_right(Node<T> *p) {
    Node<T>* q = p->left;
    p->left = q->right;
    q->right = p;

    fixNodes(p);
    fixNodes(q);

    fixHeight(q);
    fixHeight(p);

    return q;
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::rotate_left(Node<T> *q) {
    Node<T>* p = q->right;
    q->right = p->left;
    p->left = q;

    fixNodes(q);
    fixNodes(p);

    fixHeight(q);
    fixHeight(p);

    return p;
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::balance(Node<T> *p) {
    fixHeight(p);
    if (bfactor(p) == 2) {
        if (bfactor(p->right) < 0) {
            p->right = rotate_right(p->right);
        }
        return rotate_left(p);
    }

    if (bfactor(p) == -2) {
        if (bfactor(p->left) > 0) {
            p->left = rotate_left(p->left);
        }
        return rotate_right(p);
    }

    return p;
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::insert(Node<T> *p, T k, int &position, Comparator cmp) {
    if(!p) {
        return new Node<T>(k);
    }

    ++(p->nodes);

    if (cmp(k, p->key)) {
        position += Nodes(p->right) + 1;
        p->left = insert(p->left, k, position);
    } else {
        p->right = insert(p->right, k, position);
    }

    return balance(p);
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::find_min(Node<T> *p) {
    return p->left ? find_min(p->left) : p;
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::remove_min(Node<T> *p) {
    if(p->left == nullptr) {
        return p->right;
    }

    p->left = remove_min(p->left);
    --(p->nodes);

    return balance(p);
}

template<typename T, typename Comparator>
Node<T> *AVL_tree<T, Comparator>::Remove(Node<T> *p, int position) {
    if (p == nullptr) {
        return nullptr;
    }

    if (position >= p->nodes) {
        return p;
    }

    int current = 0;
    std::stack<Node<T> *> nodes;

    while(true) {
        int nodes_right = Nodes(p->right);

        // Левее корня дерева
        if (position - current > nodes_right) {
            nodes.push(p);
            p = p->left;
            current += nodes_right + 1;
            // Если лежит правее
        } else if (position - current < nodes_right) {
            if (p->right != nullptr) {
                nodes.push(p);
                p = p->right;
            } else {
                break;
            }
        } else {
            // Победа! Нашли место
            Node<T> *left = p->left;
            Node<T> *right = p->right;
            int key = p->key;

            delete p;

            // если не было у удаляемого эл-та дочерних, то
            // просто вытаскиваем предшествующий ему из стека
            if (right == nullptr) {
                //если не было левого
                if (left == nullptr) {
                    if (!nodes.empty()) {
                        p = nodes.top();
                        nodes.pop();
                        //налаживаем связи
                        if (p->key > key) {
                            p->left = nullptr;
                        } else {
                            p->right = nullptr;
                        }

                        --(p->nodes);
                    } else {
                        return nullptr;
                    }
                } else {
                    p = left;
                }
            } else {
                Node<T> *min = find_min(right);
                min->right = remove_min(right);
                min->left = left;
                fixNodes(min);
                p = balance(min);
            }
            break;
        }
    }

    // вытаскиваем все из стека обратно
    while (!nodes.empty()) {
        Node<T> *node = nodes.top();
        --(node->nodes);

        if (node->key > p->key) {
            node->left = p;
        } else {
            node->right = p;
        }

        p = balance(node);
        nodes.pop();
    }

    return p;
}

enum OP {
    ADD = 1,
    DELETE = 2
};

int main()
{
    int n;
    std::cin >> n;

    Node<int>* root = nullptr;
    std::vector<int> result;

    int cmd, key, position;

    AVL_tree<int> my_tree;
    for(size_t i = 0; i < n; ++i ) {
        std::cin >> cmd;
        switch (cmd) {
            case ADD:
                position = 0;
                std::cin >> key;
                root = my_tree.insert(root, key, position);
                result.push_back(position);
                break;
            case DELETE:
                std::cin >> key;
                root = my_tree.Remove(root, key);
                break;
            default:
                break;
        }
    }

    for(int i : result) {
        std::cout << i << std::endl;
    }

    return 0;
}
