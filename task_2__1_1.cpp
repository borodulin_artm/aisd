/*
 * Реализуйте структуру данных типа “множество строк” на основе динамической хеш-таблицы с открытой адресацией.
 * Хранимые строки непустые и состоят из строчных латинских букв.
 * Хеш-функция строки должна быть реализована с помощью вычисления значения многочлена методом Горнера.
 * Начальный размер таблицы должен быть равным 8-ми.
 * Перехеширование выполняйте при добавлении элементов в случае, когда коэффициент заполнения таблицы достигает 3/4.
 * Структура данных должна поддерживать операции добавления строки в множество, удаления строки из множества и проверки принадлежности данной строки множеству. 1_1.
 * Для разрешения коллизий используйте квадратичное пробирование. i-ая проба g(k, i)=g(k, i-1) + i (mod m). m - степень двойки.
 */

#include <iostream>
#include <vector>

#define MULTIPLICATOR 2
#define FILL_TO_EXPAND 0.8

enum TYPE_OPERATION {
    ADD_TO_TABLE = '+',
    REMOVE_FROM_TABLE = '-',
    CHECK_FOR_CONTAINS = '?'
};

// Структура данных "Узел хеш-таблицы"
template<typename T>
struct Node {
    T key;
    bool is_deleted;

    // Будем помечать отдельным флагом "удалён"
    Node(): is_deleted(false), key("") {}

    // Проверка на то, что ключ пуст
    bool is_empty() const {
        return key == "";
    }
};

template<typename T>
class HashTable {
public:
    // Конструторы
    HashTable();

    bool insert(T key);
    bool remove(T key);
    bool contains(T key);

    template<typename T1>
    friend std::ostream &operator<< (std::ostream &out, const HashTable<T1> &h_table);
private:
    // Приватные поля
    size_t current_size_hash_table;
    std::vector<Node<T>> buffer;

    int hash(T key);
    void rehash();
    double get_filling_hash_table();
};

template<typename T>
HashTable<T>::HashTable(): current_size_hash_table(0), buffer(8) {
}

template<typename T>
bool HashTable<T>::insert(T key) {
    // Поскольку адресация открытая, то нужен индекс,
    // который будет перемещать нашу хеш-таблицу
    int i = 0;
    int _hash = hash(key);
    int index = _hash;

    do {
        _hash = (_hash + i * i) % buffer.size();

        if (!buffer[index].is_empty() || !buffer[index].is_deleted) {
            index = _hash;
        }

        ++i;
        // Зачем что-либо вставлять, если это уже вставлено
        if (buffer[_hash].key == key) {
            return false;
        }
    } while (buffer[_hash].is_deleted || !buffer[_hash].is_empty());

    buffer[index].key = key;
    buffer[index].is_deleted = false;
    ++current_size_hash_table;

    // Если таблица заполнена больше, чем на 80%, то
    // перехешируем её
    if (get_filling_hash_table() >= FILL_TO_EXPAND) {
        rehash();
    }

    return true;
}

template<typename T>
bool HashTable<T>::remove(T key) {
    int i = 1;
    int _hash = hash(key);

    while (true) {
        // Если нашли хороший элемент (он есть в нашей таблице),
        // то просто удаляем его, без каких-либо вопросов
        if (buffer[_hash].key == key && !buffer[_hash].is_deleted) {
            buffer[_hash].is_deleted = true;
            buffer[_hash].key = "";
            return true;
        }

        // Плохой случай. Если элемента нет, при этом ячейка пуста
        if (buffer[_hash].is_empty() && !buffer[_hash].is_deleted) {
            return false;
        }

        // Условие выхода
        if (i == buffer.size()) {
            return false;
        }

        _hash = (_hash + i * i) % buffer.size();
        ++i;
    }
}

// Метод осуществляет поиск по заданному ключу
template<typename T>
bool HashTable<T>::contains(T key) {
    int i = 1;
    // Вычисление хеша
    int _hash = hash(key);
    while (true) {
        // Если ключ найден при этом он не удалён
        if (buffer[_hash].key == key && !buffer[_hash].is_deleted) {
            return true;
        }

        // Если это вообще пустая ячейка, то false, поскольку в случае добавления
        // данного значения она была бы вставлена именно в это место
        if (buffer[_hash].is_empty() && !buffer[_hash].is_deleted) {
            return false;
        }

        if (i == buffer.size()) {
            return false;
        }

        _hash = (_hash + i * i) % buffer.size();
        ++i;
    }
}

// Вычисление хеша по заданной строке. Используется метод Горнера
template<typename T>
int HashTable<T>::hash(T key) {
    int _hash = 0;
    for(size_t i = 0; i < key.size(); ++i) {
        _hash = (key[i] * 71 + _hash) % buffer.size();
    }

    return _hash;
}

template<typename T>
void HashTable<T>::rehash() {
    // Создаём новый буфер
    size_t new_buff_size = buffer.size() * MULTIPLICATOR;
    std::vector<Node<T>> new_buff(new_buff_size);
    std::vector<Node<T>> old_buff = buffer;

    // Перехешируем каждый из элементов
    current_size_hash_table = 0;
    // Копируем новый буфер, поскольку мы в него будем записывать данные
    buffer = new_buff;

    for(size_t i = 0; i < old_buff.size(); ++i) {
        if (!old_buff[i].is_empty() && !old_buff[i].is_deleted) {
            insert(old_buff[i].key);
        }
    }
}

// Метод возвращает степень наполненнсоти хеш-таблицы
template<typename T>
double HashTable<T>::get_filling_hash_table() {
    return (double)(current_size_hash_table) / (double)(buffer.size());
}

// Перегруженный оператор вывода
template<typename T1>
std::ostream &operator<<(std::ostream &out, const HashTable<T1> &h_table) {
    for(size_t i = 0; i < h_table.size_hash_table; ++i) {
        if (h_table.buffer[i].is_empty()) {
            out << i << ": empty\n";
        } else if (h_table.buffer[i].is_deleted) {
            out << i << ": deleted\n";
        } else {
            out << i << ": " << h_table.buffer[i].key << std::endl;
        }
    }
    return out;
}

int main() {
    HashTable<std::string> hashTable;

    std::string data_to_add;
    char type_command = 0;
    bool result_executing;

    // Вводим в цикл наши данные
    while (std::cin >> type_command >> data_to_add) {
        switch (type_command) {
            // Если введён '+'
            case ADD_TO_TABLE:
                result_executing = hashTable.insert(data_to_add);
                break;

            // Если введён '-'
            case REMOVE_FROM_TABLE:
                result_executing = hashTable.remove(data_to_add);
                break;

            // Если введён '?'
            case CHECK_FOR_CONTAINS:
                result_executing = hashTable.contains(data_to_add);
                break;

            default:
                break;

        }

        // Выводит ОК, если операция вернула true
        // False в противном случае
        if (result_executing) {
            std::cout << "OK\n";
        } else {
            std::cout << "FAIL\n";
        }
    }
}
