/*
 * Для сложения чисел используется старый компьютер. Время, затрачиваемое на нахождение суммы двух чисел равно их сумме.
 * Таким образом для нахождения суммы чисел 1,2,3 может потребоваться разное время, в зависимости от порядка вычислений.
 * ((1+2)+3) -> 1+2 + 3+3 = 9 ((1+3)+2) -> 1+3 + 4+2 = 10 ((2+3)+1) -> 2+3 + 5+1 = 11
 * Требуется написать программу, которая определяет минимальное время, достаточное для вычисления суммы заданного набора чисел. Требуемое время работы O(n*log(n)).
*/

#include <cassert>
#include <iostream>

// В этом классе будет реализована куча целых чисел
template<typename T, typename Comparator = std::less<int>>
class HeapIntegers {
private:
    T* array;
    int count_filling;
    size_t size_array;

    Comparator comparator;

public:
    explicit HeapIntegers(int size );
    ~HeapIntegers();

    T GetFirstElement();
    [[nodiscard]] int GetCountFilling() const;
    void SiftDown(int depth);
    void SiftUp(int high);
    void AddToHeap(T number);

    void swap_elements(int first_index, int second_index);
};

// Инициализация массива
template<typename T, typename Comparator>
HeapIntegers<T, Comparator>::HeapIntegers(int size): size_array(size), count_filling(0) {
    array = new T[size_array];
}

template<typename T, typename Comparator>
HeapIntegers<T, Comparator>::~HeapIntegers() {
    delete array;
}

template<typename T, typename Comparator>
T HeapIntegers<T, Comparator>::GetFirstElement() {
    T temp = array[0];
    swap_elements(0, --count_filling);
    SiftDown( 0 );
    return temp;
}

template<typename T, typename Comparator>
void HeapIntegers<T, Comparator>::SiftDown(int depth) {
    int left = 2 * depth + 1;
    int right = 2 * depth + 2;

    // Поиск дочернего элемента
    int largest = depth;

    if(left < count_filling && comparator(array[left], array[depth])) {
        largest = left;
    }

    if(right < count_filling && comparator(array[right], array[largest])) {
        largest = right;
    }

    // Если есть, то проталкиваем корень в него.
    if(largest != depth ) {
        swap_elements(depth, largest);
        this->SiftDown(largest);
    }
}

template<typename T, typename Comparator>
void HeapIntegers<T, Comparator>::SiftUp(int high) {
    int queen_value = 0;

    while(high > 0 ) {
        queen_value = (high - 1 ) / 2;
        if(comparator(array[high], array[queen_value])) {
            swap_elements(high, queen_value);
        }

        high = queen_value;
    }
}

template<typename T, typename Comparator>
void HeapIntegers<T, Comparator>::AddToHeap(T number) {
    array[count_filling] = number;
    SiftUp(count_filling);
    count_filling++;
}

// Обмен элементами внутри массива
template<typename T, typename Comparator>
void HeapIntegers<T, Comparator>::swap_elements(int first_index, int second_index) {
    T temp = array[first_index];
    array[first_index] = array[second_index];
    array[second_index] = temp;
}

// Геттер
template<typename T, typename Comparator>
int HeapIntegers<T, Comparator>::GetCountFilling() const {
    return count_filling;
}

// Бенчмарк (подсчёт времени)
template<typename T, typename Comparator = std::less<int>>
int BenchTime(HeapIntegers<T, Comparator>* array) {
    int summa = 0, current_time = 0;

    while (array->GetCountFilling() > 1) {
        summa = array->GetFirstElement();
        summa += array->GetFirstElement();

        array->AddToHeap(summa);
        current_time += summa;
    }

    return current_time;
}

int main() {
    int count_numbers = 0;
    std::cin >> count_numbers;
    assert(count_numbers >= 0);

    HeapIntegers<int> my_integers(count_numbers);

    int current_value = 0;
    for(int i = 0; i < count_numbers; ++i) {
        std::cin >> current_value;
        my_integers.AddToHeap(current_value);
    }

    std::cout << BenchTime(&my_integers) << std::endl;
    return 0;
}
