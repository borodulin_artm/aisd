#include <iostream>
#include <stack>

int main() {
    int size;
    int *mass;

    std::cin >> size;
    int temp;
    int min = 0;
    std::stack<int> val;
    for(size_t i = 0; i < size; ++i) {
        std::cin >> temp;
        if (i == 0) {
            min = temp;
            val.push(i);
        } else {
            if (temp < min) {
                min = temp;
                val.push(i);
            }
        }
    }
    std::cout << val.top() << std::endl;
}