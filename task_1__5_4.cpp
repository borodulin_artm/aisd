/*
 * На числовой прямой окрасили N отрезков.
 * Известны координаты левого и правого концов каждого отрезка [Li, Ri].
 * Найти сумму длин частей числовой прямой, окрашенных ровно в один слой.
 * N ≤ 10000. Li, Ri — целые числа в диапазоне [0, 10^9].
 */

#include <cassert>
#include <cstring>
#include <iostream>
#include <map>

class Point {
public:
    // Координата
    long x;
    // Начало
    bool start;
    explicit Point(long _x = 0, bool _start = false);

    bool operator < (const Point &other) const;
};

Point::Point(long _x, bool _start) : x(_x), start(_start) {
}

bool Point::operator < (const Point &other) const {
    return this->x < other.x;
}

// Слияние двух массивов
template<class T, typename Comparator = std::less<T>>
void Merge(T *first_mass, size_t length_first, T *second_mass, size_t length_second, T *temp_array, Comparator cmp = Comparator()) {
    int i = 0, j = 0;
    for (int current_index = 0; current_index < length_first + length_second; current_index++) {
        if (j >= length_second || (cmp(first_mass[i], second_mass[j]) && i < length_first )) {
            temp_array[current_index] = first_mass[i];
            ++i;
        } else {
            temp_array[current_index] = second_mass[j];
            ++j;
        }
    }
}

// Сортировка слиянием
template<class T>
void MergeSort(T *mass, size_t length_arr) {
    if (length_arr <= 1) {
        return;
    }

    size_t firstLen = length_arr / 2;
    size_t secondLen = length_arr - firstLen;

    MergeSort(mass, firstLen);
    MergeSort(mass + firstLen, secondLen);

    T *temp_arr = new T[length_arr];
    Merge(mass, firstLen, mass + firstLen, secondLen, temp_arr);

    memcpy(mass, temp_arr, sizeof(T) * length_arr);
    delete [] temp_arr;
}


// Подсчёт длины с одного слоя
int GetSingleLayerNumber(Point *points, size_t n) {
    int result = 0;

    MergeSort<Point>(points, n);

    long lastStart = -1;
    int curLayers = 0;

    for(size_t i = 0; i < n; i++) {
        bool one_layer = (curLayers == 1);

        if (points[i].start) {
            curLayers += 1;
        } else {
            curLayers -= 1;
        }

        if(curLayers == 1) {
            lastStart = points[i].x;
        } else if(one_layer) {
            result += (int)(points[i].x - lastStart);
        }
    }

    return result;
}

int main() {
    size_t n = 0;
    std::cin >> n;
    assert(n >= 0);

    n *= 2;
    auto *points = new Point[n];

    for(size_t i = 0; i < n; i++) {
        std::cin >> points[i].x;
        points[i].start = (i % 2 == 0);
    }

    std::cout << GetSingleLayerNumber(points, n);

    delete[] points;
    return 0;
}
