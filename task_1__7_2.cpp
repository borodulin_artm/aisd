/*
 * Дан массив неотрицательных целых 64-битных чисел. Количество чисел не больше 1000000.
 * Отсортировать массив методом поразрядной сортировки LSD по байтам.
 */

#include <iostream>
#include <cstring>

#define COUNTER_SIZE 256

// Извлечение текущего байта числа
int getByte(long long num, size_t n) {
    return num >> (8 * n) & 255;
}

// Сортируем подсчётом (работает быстро) по конкретному байту
void countingSort(long long *data, size_t size, const size_t n) {
    size_t counters[COUNTER_SIZE];

    for(unsigned long & counter : counters) {
        counter = 0;
    }

    // считаем количество чисел с разными значениями байта (от 0 до 255)
    for(size_t i = 0; i < size; i++) {
        // Для каждого из элементов извлекаем байт и записываем кол-во найденных чисел в массив
        counters[getByte(data[i], n)]++;
    }

    // расчитываем первые индексы для вставки чисел
    for(size_t i = 1; i < COUNTER_SIZE; i++) {
        counters[i] += counters[i - 1];
    }

    // массив для результатов
    auto *tmp = new long long[size];

    // создаем отсортированный массив результатов
    for(size_t i = size - 1;; i--) {
        tmp[--counters[getByte(data[i], n)]] = data[i];
        if (i == 0) {
            break;
        }
    }

    // Копируем из tmp в data
    memcpy(data, tmp, size * sizeof(long long));
    delete[] tmp;
}

// сортировка LSD
void LSDSort(long long *data, size_t size) {
    for(size_t byte = 0; byte < sizeof(long long); ++byte) {
        countingSort(data, size, byte);
    }
}

int main() {
    size_t size_array = 0;
    std::cin >> size_array;

    auto *data = new long long[size_array];

    // Ввод массива
    for(size_t i = 0; i < size_array; i++) {
        std::cin >> data[i];
    }

    // Сортировка
    LSDSort(data, size_array);

    // Вывод массива
    for(size_t i = 0; i < size_array; i++) {
        std::cout << data[i] << " ";
    }

    delete[] data;
    return 0;
}
