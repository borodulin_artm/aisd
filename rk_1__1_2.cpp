#include <iostream>

using namespace std;

class my_stack {
private:
    int *stack;
    int size, count_values;

    void IncrementSize() {
        size *= 2;
        stack = (int*)std::realloc(stack, count_values * 2);
    }

public:
    my_stack() {
        size = 16;
        stack = new int[size];
        count_values = 0;
    }

    ~my_stack() {
        delete[] stack;
    }

    bool is_empty() const {
        return count_values == 0;
    }

    void insert(int value) {
        if (count_values >= size)
            IncrementSize();

        stack[count_values] = value;
        count_values++;
    }

    int remove() {
        int value = -1;
        if (!is_empty()) {
            count_values--;
            value = stack[count_values];
        }

        return value;
    }
};

bool check_for_good(string sequence) {
    my_stack chars;
    int i = 0;
    while (i < sequence.size()) {
        if (sequence[i] == '(')
            chars.insert(1);
        else if (chars.remove() == -1)
            return false;

        ++i;
    }

    if (chars.is_empty())
        return true;
    else
        return false;
}

int main() {
    string sequence;
    cin >> sequence;
    if (check_for_good(sequence))
        cout << "YES";
    else
        cout << "NO";
    return 0;
}