/*
 * Вариант 3
 * Даны два строго возрастающих массива целых чисел A[0..n) и B[0..m) и число k.
 * Найти количество таких пар индексов (i, j), что A[i] + B[j] = k.
 * Время работы O(n + m). n, m ≤ 100000.
 * Указание. Обходите массив B от конца к началу.
 */

#include <iostream>
#include <cassert>

int countIndexes(int length_first, const int *first_array, int second_length, const int *second_array, int k) {
    int count = 0;

    // обход B
    int last_j = 0; // последний ключ в A
    for(int i = second_length - 1; i >= 0; i--) {
        int value_from_second_arr = second_array[i];

        for(int j = last_j; j < length_first; j++) {
            int sum = value_from_second_arr + first_array[j];
            if(sum == k) {
                count++;
            } else if(sum > k) {
                last_j = j;
                break;
            }
        }
    }

    return count;
}

int main() {
    int length_first = 0, length_second = 0, *first_arr = nullptr, *second_arr = nullptr;

    // ввод длины массива А
    std::cin >> length_first;
    assert(length_first >= 0);

    // ввод массива А
    first_arr = new int[length_first];
    for(int i = 0; i < length_first; i++) {
        std::cin >> first_arr[i];
    }

    // ввод длины массива B
    std::cin >> length_second;
    assert(length_second >= 0);

    // ввод массива B
    second_arr = new int[length_second];
    for(int i = 0; i < length_second; i++) {
        std::cin >> second_arr[i];
    }

    // ввод k
    int k = 0;
    std::cin >> k;

    // подсчет индексов и вывод
    int result = countIndexes(length_first, first_arr, length_second, second_arr, k);
    std::cout << result;

    // очистка памяти

    delete[] first_arr;
    delete[] second_arr;

    return 0;
}
