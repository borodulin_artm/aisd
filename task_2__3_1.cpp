/*
 * Дано число N < 106 и последовательность пар целых чисел из [-231, 231] длиной N.
 * Построить декартово дерево из N узлов, характеризующихся парами чисел (Xi, Yi).
 * Каждая пара чисел (Xi, Yi) определяет ключ Xi и приоритет Yi в декартовом дереве.
 * Добавление узла в декартово дерево выполняйте следующим образом:
 * При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим приоритетом.
 * Затем разбейте найденное поддерево по ключу x так, чтобы в первом поддереве все ключи меньше x,
 * а во втором больше или равны x. Получившиеся два дерева сделайте дочерними для нового узла (x, y).
 * Новый узел вставьте на место узла P.
 * Построить также наивное дерево поиска по ключам Xi.
 * Т.е., при добавлении очередного числа K в наивное дерево с корнем root, если root→Key ≤ K,
 * то узел K добавляется в правое поддерево root; иначе в левое поддерево root.
 * Вычислить разницу глубин наивного дерева поиска и декартового дерева.
 * Разница может быть отрицательна, необходимо вывести модуль разности.
 */

#include <iostream>
#include <vector>
#include <queue>
#include <cassert>

// Структура текущего узла
template<typename T>
struct Node {
    T data;
    // Здесь также добавляется приоритет для декартова дерева
    int priority;
    Node* left,* right;
    Node(int Key, int Priority, Node* Left, Node* Right): data(Key), priority(Priority),
                                                          left(Left), right(Right) {}
    explicit Node(int key): data(key), priority(0), left(nullptr), right(nullptr) {}
};

// Структура Бинарного дерева
template<typename T, typename Comparator = std::less_equal<T>>
class BinaryTree {
public:
    explicit BinaryTree();
    ~BinaryTree();
    void add(int key, Comparator cmp = Comparator());
    Node<T> *get_root() const;
private:
    Node<T>* root;
};

template<typename T, typename Comparator>
BinaryTree<T, Comparator>::BinaryTree(): root(nullptr) {}

template<typename T, typename Comparator>
BinaryTree<T, Comparator>::~BinaryTree() {
    std::queue<Node<T>*> queue_to_delete;

    queue_to_delete.push(root);
    while(!queue_to_delete.empty()) {
        Node<T> *node = queue_to_delete.front();
        if (node->left) {
            queue_to_delete.push(node->left);
        }

        if (node->right) {
            queue_to_delete.push(node->right);
        }

        delete node;
        queue_to_delete.pop();
    }
}

// Добавления ключа в дерево
template<typename T, typename Comparator>
void BinaryTree<T, Comparator>::add(int key, Comparator cmp) {
    if (root == nullptr) {
        root = new Node<T>(key);
    } else {
        Node<T> *current = root;
        while (true) {
            // Если ключ больше, чем данное значение,
            // то идём вправо, иначе - идём влево
            if (cmp(current->data, key)) {
                if (current->right == nullptr) {
                    current->right = new Node<T>(key);
                    break;
                } else {
                    current = current->right;
                }
            } else {
                if (current->left == nullptr) {
                    current->left = new Node<T>(key);
                    break;
                } else {
                    current = current->left;
                }
            }
        }
    }
}

template<typename T, typename Comparator>
Node<T> *BinaryTree<T, Comparator>::get_root() const {
    return root;
}

// Структура данных "Декартово дерево"
template<typename T, typename Comparator = std::less_equal<T>>
class DecartTree {
public:
    explicit DecartTree(Node<T>* node);
    ~DecartTree();

    void add(Node<T>*& node, int key, int priority, Comparator cmp = Comparator());
    void split(Node<T>* node, int key, Node<T>*& left, Node<T>*& right, Comparator cmp = Comparator());
    Node<T>* get_root() const;
private:
    Node<T>* root;
};

template<typename T, typename Comparator>
DecartTree<T, Comparator>::DecartTree(Node<T> *node): root(node) {}

template<typename T, typename Comparator>
Node<T>* DecartTree<T, Comparator>::get_root() const {
    return root;
}

template<typename T, typename Comparator>
DecartTree<T, Comparator>::~DecartTree() {
    std::queue<Node<T>*> queue_to_delete;

    queue_to_delete.push(root);
    while(!queue_to_delete.empty()) {
        Node<T> *node = queue_to_delete.front();
        if (node->left) {
            queue_to_delete.push(node->left);
        }

        if (node->right) {
            queue_to_delete.push(node->right);
        }

        delete node;

        queue_to_delete.pop();
    }
}

template<typename T, typename Comparator>
void DecartTree<T, Comparator>::split(Node<T>* node, int key, Node<T>*& left, Node<T>*& right, Comparator cmp) {
    if (!node) {
        left = right = nullptr;
    } else {
        if (cmp(node->data, key)) {
            split(node->right, key, node->right, right);
            left = node;
        } else {
            split(node->left, key, left, node->left);
            right = node;
        }
    }
}

// Добавление данных
template<typename T, typename Comparator>
void DecartTree<T, Comparator>::add(Node<T>*& node, int key, int priority, Comparator cmp) {
    if (node == nullptr) {
        node = new Node<T>(key, priority, nullptr, nullptr);
        return;
    } else if (cmp(node->priority, priority)) {
        Node<T> *Left, *Right;
        split(node, key, Left, Right);
        if (node == root) {
            root = new Node<T>(key, priority, Left, Right);
        } else {
            node = new Node<T>(key, priority, Left, Right);
        }
        return;
    } else {
        if (!cmp(node->data, key)) {
            add(node->left, key, priority);
        } else {
            add(node->right, key, priority);
        }
    }
}

template<typename T, class U>
int getDepth(U &data) {
    std::vector<Node<T>*> current_depth, temp;
    current_depth.push_back(data.get_root());

    int maxDepth = 0;
    while(!current_depth.empty()) {
        for (int i = 0; i < current_depth.size(); ++i) {
            if (current_depth[i]->left != nullptr) {
                temp.push_back(current_depth[i]->left);
            }
            if (current_depth[i]->right != nullptr) {
                temp.push_back(current_depth[i]->right);
            }
        }
        current_depth.clear();
        current_depth = temp;

        temp.clear();
        if (!current_depth.empty()) {
            ++maxDepth;
        }
    }

    return maxDepth;
}

int main() {
    int amount, key, priority;

    std::cin >> amount;
    assert(amount > 0);

    std::cin >> key >> priority;
    auto* decnode = new Node<int>(key, priority, nullptr, nullptr);

    BinaryTree<int> tree;
    DecartTree<int> decTree(decnode);

    tree.add(key);
    for (int i = 1; i < amount; ++i) {
        std::cin >> key >> priority;
        tree.add(key);

        decnode = decTree.get_root();
        decTree.add(decnode, key, priority);
    }

    std::cout << abs(getDepth<int, BinaryTree<int>>(tree) - getDepth<int, DecartTree<int>>(decTree));
    return 0;
}
