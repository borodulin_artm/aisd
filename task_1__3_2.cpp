/*
 *  Реализовать дек с динамическим зацикленным буфером.
 *  Обрабатывать команды push * и pop *.
 *  Формат ввода
 *  В первой строке количество команд n. n ≤ 1000000.
 *  Каждая команда задаётся как 2 целых числа: a b.
 *  a = 1 - push front
 *  a = 2 - pop front
 *  a = 3 - push back
 *  a = 4 - pop back
 *  Если дана команда pop *, то число b - ожидаемое значение.
 *  Если команда pop * вызвана для пустой структуры данных,
 *  то ожидается “-1”.
 */

#include <iostream>
#include <cstring>
#include <cassert>

enum TYPE_COMMAND {
    PUSH_FRONT = 1,
    POP_FRONT = 2,
    PUSH_BACK = 3,
    POP_BACK = 4
};

//  Реализация дека с динамическим зацикленным буфером
template <typename T>
class Deque {
public:
    Deque();
    ~Deque();

    // Удаление с конца и начала дека соответсвенно
    T Pop_Back();
    T Pop_Front();

    // Добавление в конец и начала дека
    void Push_Back(T value);
    void Push_Front(T value);

    // Проверка на заполненность дека
    [[nodiscard]] bool Is_Full() const;
    [[nodiscard]] bool Is_Empty() const;

private:
    T *buffer;
    int head;
    int tail;
    size_t capacity;

    //увеличение размера дэка в 2 раза
    void memory_allocation();
};

template<typename T>
Deque<T>::Deque(): buffer(new T[4]), head(0), tail(0), capacity(4) {
}

template<typename T>
Deque<T>::~Deque() {
    delete[] buffer;
}

template<typename T>
bool Deque<T>::Is_Empty() const {
    return tail == head;
}

template<typename T>
bool Deque<T>::Is_Full() const {
    return tail + 1 == head || (tail + 1 == capacity && head == 0);
}

template <typename T>
void Deque<T>::memory_allocation() {
    auto *new_buf = new T[capacity * 2];
    assert(new_buf != nullptr);

    if (head > tail) {
        memcpy(new_buf + head, buffer + head, (capacity - head) * sizeof(T));
        memcpy(new_buf + capacity, buffer, tail * sizeof(T));
        tail += capacity;
    } else {
        memcpy(new_buf, buffer, (tail - head) * sizeof(T));
    }

    delete [] buffer;
    buffer = new_buf;
    capacity *= 2;
};

template <typename T>
void Deque<T>::Push_Back(T const value) {
    if (Is_Full()) {
        memory_allocation();
    }

    buffer[tail++] = value;
    tail %= capacity;
}

template <typename T>
void Deque<T>::Push_Front(T value) {
    if (Is_Full()) {
        memory_allocation();
    }

    if (Is_Empty()) {
        buffer[head] = value;
        ++tail;
        tail %= capacity;
    } else {
        if (head == 0) {
            head = capacity;
        }

        buffer[--head] = value;
        head %= capacity;
    }
}

template <typename T>
T Deque<T>::Pop_Back() {
    if (Is_Empty()) {
        return -1;
    }

    if (tail == 0) {
        tail = capacity;
    }

    --tail %= capacity;
    return buffer[tail];
}

template <typename T>
T Deque<T>::Pop_Front() {
    if (Is_Empty()) {
        return -1;
    }

    int buf_value =  buffer[head];
    head = (head + 1) % capacity;
    return buf_value;
}

int main() {
    Deque<int> deq;

    int count_values, command, value;
    bool flag = true;

    std::cin >> count_values;

    for (size_t i = 0; i < count_values && flag; ++i) {
        std::cin >> command >> value;
        switch (command) {
            case PUSH_FRONT:
                deq.Push_Front(value);
                break;

            case POP_FRONT:
                if (deq.Is_Empty() && value != -1 || value != deq.Pop_Front()) {
                    flag = false;
                }
                break;

            case PUSH_BACK:
                deq.Push_Back(value);
                break;

            case POP_BACK:
                if (deq.Is_Empty() && value != -1 || value != deq.Pop_Back()) {
                    flag = false;
                }

                break;

            default:
                break;
        }
    }

    if (!flag) {
        std::cout << "NO";
    } else {
        std::cout << "YES";
    }

    return 0;
}