/*
 * Даны два массива неповторяющихся целых чисел, упорядоченные по возрастанию. A[0..n-1] и B[0..m-1]. n » m.
 * Найдите их пересечение. Требуемое время работы:
 * O(m * log k), где k - позиция элементта B[m-1] в массиве A..
 * В процессе поиска очередного элемента B[i] в массиве A пользуйтесь результатом поиска элемента B[i-1].
 * n, k ≤ 10000.
 */

#include <cassert>
#include <iostream>

// Реализация класса массив
class Array {
private:
    int *mass;
    int used_size;
    int size_mass;

    void initalize_array(int size_array);

public:
    Array();
    Array(int size_array);
    Array(const Array &);
    ~Array();

    [[nodiscard]] int get_used() const;
    [[nodiscard]] int *get_mass() const;

    void append(int element);
    friend std::ostream &operator <<(std::ostream &out, const Array &arr);
};

void Array::initalize_array(int size_array) {
    mass = (int*) malloc(size_array * sizeof(int));
    if (mass == nullptr) {
        return;
    }

    used_size = 0;
    size_mass = size_array;
}

Array::Array() {
    // Инициализация с дефолтным значением
    initalize_array(10);
}

Array::Array(int size_array) {
    initalize_array(size_array);
}

Array::Array(const Array &copy_chunk) {
    mass = (int *) malloc(copy_chunk.used_size * sizeof(int));
    if (mass == nullptr) {
        return;
    }

    for (int i = 0; i < copy_chunk.used_size; ++i) {
        this->mass[i] = copy_chunk.mass[i];
    }

    this->used_size = copy_chunk.used_size;
};

void Array::append(int element) {
    if (used_size == size_mass) {
        int required_size = size_mass * 2;
        auto new_array = (int *) realloc(mass, required_size * sizeof(int));
        if (new_array == nullptr) {
            return;
        }

        mass = new_array;
        size_mass = required_size;
    }

    mass[used_size] = element;
    ++used_size;
}

Array::~Array() {
    free(mass);
}

int Array::get_used() const {
    return used_size;
}

int *Array::get_mass() const {
    return mass;
}

std::ostream &operator<<(std::ostream &out, const Array &arr) {
    for(int i = 0; i < arr.used_size; ++i) {
        out << arr.mass[i] << " ";
    }

    return out;
}

int binary_search(const int *array, int first_index, int last_index, const int value){
    if (first_index == last_index || value < array[first_index] || array[last_index - 1] < value) {
        return -1;
    }

    // Переводя на простой язык, можно сказать, что на данном интервале есть значения
    while (first_index < last_index) {
        int center = first_index + (last_index - first_index) / 2;
        if (value <= array[center])
            last_index = center;
        else
            first_index = center + 1;
    }

    if (array[last_index] == value) {
        // Искомый элемент найден.
        return last_index;
    } else {
        return -1;
    }
}

Array intersection_search(const Array &array_A, const Array &array_B) {
    Array to_return;
    int left_bound = 0, right_bound = 1;

    for(int i = 0; i < array_B.get_used(); ++i){
        while (array_A.get_mass()[right_bound - 1] < array_B.get_mass()[i] && right_bound < array_A.get_used()){
            right_bound = (right_bound * 2 > array_A.get_used()) ? array_A.get_used() : right_bound * 2;
        }

        int index = binary_search(array_A.get_mass(), left_bound, right_bound, array_B.get_mass()[i]);
        if(index != -1){
            left_bound = index;
            to_return.append(array_A.get_mass()[index]);
        }
    }

    return to_return;
};

int main() {
    int size_array_A = 0, size_array_B = 0;

    std::cin >> size_array_A;
    std::cin >> size_array_B;

    assert(size_array_A >= 0);
    assert(size_array_B >= 0);

    Array array_A(size_array_A), array_B(size_array_B);

    int tmp = 0;
    for(int i = 0; i < size_array_A; ++i){
        std::cin >> tmp;
        array_A.append(tmp);
    }

    for(int i = 0; i < size_array_B; ++i){
        std::cin >> tmp;
        array_B.append(tmp);
    }

    Array result = intersection_search(array_A, array_B);
    std::cout << result << std::endl;
    return 0;
}