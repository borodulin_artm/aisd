/*
 * Дано число N < 10^6 и последовательность целых чисел из [-231..231] длиной N.
 * Требуется построить бинарное дерево поиска, заданное наивным порядком вставки. Т.е.,
 * при добавлении очередного числа K в дерево с корнем root, если root→Key ≤ K, то узел K добавляется в правое поддерево root; иначе в левое поддерево root.
 * Выведите элементы в порядке level-order (по слоям, “в ширину”).
 */

#include <iostream>
#include <cassert>
#include <queue>

// Структур данных "узел"
template<typename T>
struct Node {
    T value;
    Node *left;
    Node *right;

    // Базовый конструктор, чтобы удобно было зписывать данные
    Node(T &elem): value(elem) {}
};

// Структура данных "Бинарное дерево"
template<typename T, typename Comparator = std::less_equal<T>>
class BinaryTree {
public:
    BinaryTree();
    ~BinaryTree();
    void add(T &data, Comparator cmp = Comparator());
    std::queue<T> get_values_wide();
private:
    Node<T> *root;
};

// В качестве деструктора используем очередь
template<typename T, typename Comparator>
BinaryTree<T, Comparator>::~BinaryTree() {
    // Используем очередь для того чтобы удалить данные
    std::queue<Node<T>*> queue_to_delete;
    queue_to_delete.push(root);

    while (!queue_to_delete.empty()) {
        Node<T> *node = queue_to_delete.front();
        if (node->left) {
            queue_to_delete.push(node->left);
        }

        if (node->right) {
            queue_to_delete.push(node->right);
        }

        delete node;
        queue_to_delete.pop();
    }
}

// Функция осуществляет добавление в узел
template<typename T, typename Comparator>
void BinaryTree<T, Comparator>::add(T &data, Comparator cmp) {
    if (root == nullptr) {
        root = new Node<T>(data);
    } else {
        Node<T> *treeNode = root;
        while (true) {
            if (cmp(treeNode->value, data)) {
                // Если правый узел пуст, то создаём его и записываем туда данные
                if (treeNode->right == nullptr) {
                    treeNode->right = new Node<T>(data);
                    break;
                } else {
                    // Если узел уже есть, то просто идём вправо
                    treeNode = treeNode->right;
                }
            } else {
                if (treeNode->left == nullptr) {
                    treeNode->left = new Node<T>(data);
                    break;
                } else {
                    treeNode = treeNode->left;
                }
            }
        }
    }
}

// Получаем значения, которые будем выводить на экран
template<typename T, typename Comparator>
std::queue<T> BinaryTree<T, Comparator>::get_values_wide() {
    std::queue<T> result;
    std::queue<Node<T>*> node_tree;
    if (root == nullptr) {
        return result;
    }

    node_tree.push(root);
    while(!node_tree.empty()) {
        Node<T> *current_node = node_tree.front();
        node_tree.pop();
        result.push(current_node->value);

        if (current_node->left) {
            node_tree.push(current_node->left);
        }

        if (current_node->right) {
            node_tree.push(current_node->right);
        }
    }

    return result;
}

template<typename T, typename Comparator>
BinaryTree<T, Comparator>::BinaryTree(): root(nullptr) {}

int main() {
    size_t count_elements = 0;
    BinaryTree<int> elements;

    std::cin >> count_elements;
    assert(count_elements > 0);

    int element = 0;
    for(size_t i = 0; i < count_elements; ++i) {
        std::cin >> element;
        elements.add(element);
    }

    auto data_to_print = elements.get_values_wide();
    while (!data_to_print.empty()) {
        std::cout << data_to_print.front() << " ";
        data_to_print.pop();
    }

    return EXIT_SUCCESS;
}
